package ua.org.oa.shevchcross.PraktiaString.task4_2.part2;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

/**
 * Created by Olya on 06.05.2018.
 */
public class MyDequeImpl<E> implements MyDeque<E> {

    transient int size = 0;
    /**
     * Pointer to first node.
     * Invariant: (first == null && last == null) ||
     * (first.prev == null && first.item != null)
     */
    transient Node<E> first;
    /**
     * Pointer to last node.
     * Invariant: (first == null && last == null) ||
     * (last.next == null && last.item != null)
     */
    transient Node<E> last;
    /**
     * Constructs an empty list.
     */
    protected transient int modCount = 0;

    @Override
    public void addFirst(E o) {
        final Node<E> f = first;
        final Node<E> newNode = new Node<>(o, null, f);
        first = newNode;
        if (f == null)
            last = newNode;
        else
            f.prev = newNode;
        size++;
        modCount++;

    }

    @Override
    public void addLast(E o) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(o, l, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
        modCount++;

    }

    private E unlinkFirst(Node<E> f) {
        // assert f == first && f != null;
        final E element = f.element;
        final Node<E> next = f.next;
        f.element = null;
        f.next = null; // help GC
        first = next;
        if (next == null)
            last = null;
        else
            next.prev = null;
        size--;
        modCount++;
        return element;
    }

    @Override
    public E removeFirst() {
        final Node<E> f = first;
        if (f == null)
            System.out.println("Лист не содержит элементов");
        return unlinkFirst(f);
    }

    private E unlinkLast(Node<E> l) {
        // assert l == last && l != null;
        final E element = l.element;
        final Node<E> prev = l.prev;
        l.element = null;
        l.prev = null; // help GC
        last = prev;
        if (prev == null)
            first = null;
        else
            prev.next = null;
        size--;
        modCount++;
        return element;
    }

    @Override
    public E removeLast() {
        final Node<E> l = last;
        if (l == null)
            System.out.println("Лист не содержит элементов");
        ;
        return unlinkLast(l);
    }

    @Override
    public E getFirst() {
        final Node<E> f = first;
        if (f == null)
            System.out.println("Лист не содержит элементов");
        return f.element;
    }

    @Override
    public E getLast() {
        final Node<E> l = last;
        if (l == null)
            System.out.println("Лист не содержит элементов");
        return l.element;
    }

    public int indexOf(Object o) {
        int index = 0;
        if (o == null) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.element == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = first; x != null; x = x.next) {
                if (o.equals(x.element))
                    return index;
                index++;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public void clear() {
        for (Node<E> x = first; x != null; ) {
            Node<E> next = x.next;
            x.element = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        first = last = null;
        size = 0;
        modCount++;
    }

    @Override
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<E> x = first; x != null; x = x.next)
            result[i++] = x.element;
        return result;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public String toString() {
        return "MyDequeImpl{" +
                "size=" + size +
                ", first=" + first.element +
                ", last=" + last.element +
                '}';
                //+ "first.Node{" +
                //"first.element=" + first.element +
               // ", first.next=" + first.next +
              //  ", first.prev=" + first.prev +
                //'}' ;
    }

    @Override
    public boolean containsAll(MyDeque deque) {
        return false;
    }

    public Iterator <E> iterator() {return new IteratorImpl(); }

    //  метод должен возвращает объект внутреннего класса IteratorImpl:

    private class IteratorImpl implements Iterator<E> {

// проверяет, есть ли следующий элемент для выборки методом next
private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;
      int  currentIndex = 0;
      private Node<E> element;




        public boolean hasNext() {
            return currentIndex < size; }

// возвращает следующий элемент
        public E next() {
            lastReturned = first;
            first = first.next;
            currentIndex++;
            return lastReturned.element;
        }
        E unlink(Node<E> x) {
            // assert x != null;
            final E element = x.element;
            final Node<E> next = x.next;
            final Node<E> prev = x.prev;

            if (prev == null) {
                first = next;
            } else {
                prev.next = next;
                x.prev = null;
            }
            if (next == null) {
                last = prev;
            } else {
                next.prev = prev;
                x.next = null;
            }

            x.element = null;
            size--;
            modCount++;
            return element;
        }
// удаляет элемент, который был возвращен ранее методом next
        public void remove() {
            if (lastReturned == null)
                throw new IllegalStateException();
         //   System.out.println(lastReturned.element);

            Node<E> lastNext = lastReturned.next;
          //  System.out.println(lastNext);
            unlink(lastReturned);
           // System.out.println( next==lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                currentIndex--;
            lastReturned = null;
            modCount++;}
    }

        private static class Node<E> {
            E element;// хранимый элемент
            Node<E> next;// ссылка на следующий элемент списка
            Node<E> prev;// ссылка на предыдущий элемент списка

            Node(E element, Node<E> prev, Node<E> next) {
                this.element = element;
                this.prev = prev;
                this.next = next;
            }


        }
    }
