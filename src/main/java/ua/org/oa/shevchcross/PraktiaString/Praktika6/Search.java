package ua.org.oa.shevchcross.PraktiaString.Praktika6;

import java.io.*;

/**
 * Created by Olya on 04.06.2018.
 */
public class Search {

    public void searchFile(String Inpath, String Outpath, String whatSearch) {
        int i = 1;
        File file = new File(Inpath);
        File[] listFile = file.listFiles();
        for (File elenent: listFile) {
            if (elenent.isFile() && elenent.getName().contains(whatSearch)) {
                String s;
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(Outpath,true));) {
                    s = elenent.getName();
                    bw.write(i++ +" "+ s+ ";" + "\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } if (elenent.isDirectory()) {
                new Thread(() -> {
                    searchFile(elenent.getPath(), Outpath, whatSearch);
                }).start(); }}}

    public void  copyFile (String copyFrom, String copyTo){
        File file = new File(copyFrom);
        File [] arraylias= file.listFiles();
        for (File element: arraylias) {
            if (element.isFile()){
                File newFile =new File(copyTo+"//"+element.getName());
                try {
                    if(newFile.createNewFile())
                        try (FileInputStream fis = new FileInputStream(element);
                             FileOutputStream fos = new FileOutputStream(newFile)){
                            byte[] buf = new byte[8];
                            int count = 0;
                            while ((count = fis.read(buf)) != -1){
                                fos.write(buf,0, count);}
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (element.isDirectory()) {
                    new Thread(() -> {
                        copyFile(element.getPath(), copyTo );
                    }).start();
                }
            }
        }
    }
}
