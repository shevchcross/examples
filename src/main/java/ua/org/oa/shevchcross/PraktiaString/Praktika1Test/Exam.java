package ua.org.oa.shevchcross.PraktiaString.Praktika1Test;

/**
 * Created by Olya on 18.04.2018.
 */
public class Exam {
    String subject;
    int grade;
    String date;
    String semestr;



    public Exam(String subject, int grade, String date, String semestr) {
        this.subject = subject;
        this.grade = grade;
        this.date = date;
        this.semestr = semestr;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSemestr() {
        return semestr;
    }

    public void setSemestr(String semestr) {
        this.semestr = semestr;
    }

    @Override
    public String toString() {
        return "Exam{" +
                "subject='" + subject + '\'' +
                ", grade=" + grade +
                ", date='" + date + '\'' +
                ", semestr='" + semestr + '\'' +
                '}';
    }
}