package ua.org.oa.shevchcross.PraktiaString.Praktika1Test;

import java.util.*;

/**
 * Created by Olya on 08.04.2018.
 */
public class Student {
    String name;
    String surname;
    ArrayList<Exam> Exam = new ArrayList<>();
    int course;
    String faculty;



    public Student(String name, String surname, int course, String faculty) {
        this.name = name;
        this.surname = surname;
        this.course = course;
        this.faculty = faculty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public ArrayList<ua.org.oa.shevchcross.PraktiaString.Praktika1Test.Exam> getExam() {
        return Exam;
    }

    public void setExam(ArrayList<ua.org.oa.shevchcross.PraktiaString.Praktika1Test.Exam> exam) {
        Exam = exam;
    }

    public int getCourse() {
        return course;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public void addExam(Exam exam){
        this.Exam.add(exam);

    }


    public int searchMaxGrade (String s) throws ExamExeption {
        int MaxGrade = 0;
        int[] gradArray;
        gradArray = new int[Exam.size()];
      //  Collections.max(Exam, Co)
         for (int i = 0; i < Exam.size(); i++) {
             Exam f = Exam.get(i);
  if (s.equalsIgnoreCase(f.getSubject()) ){
           gradArray[i] = f.getGrade();} }

        //  throw new ExamExeption("Студент не сдавал такой экзамен поэтому максимаьную оценку вывести не могу");}

        for(int i = 0; i < gradArray.length; i++) {
            if(gradArray[i] > MaxGrade)
                MaxGrade = gradArray[i];}
        System.out.println(MaxGrade);

                return MaxGrade;

        }

    public void delGrade(String s, String d) {
       int Grade = 0;
                 for (int i = 0; i <Exam.size(); i++) {
                     Exam f = Exam.get(i);
                     if (s.equalsIgnoreCase(f.getSubject())&& d.equalsIgnoreCase(f.getDate())){
                         f.setGrade(Grade);
                         }}
    }

    public int valueExamWithGrade(int g){
         int valueExam = 0;
        for (int i = 0; i <Exam.size() ; i++) {
           Exam ob =  Exam.get(i);
            if (ob.getGrade() == g){
                valueExam++;
                     } }
    return  valueExam; }



         public double averageRating (String sem)              {
        double averageRat = 0;
        int a=0;
             for (int i = 0; i <Exam.size(); i++) {
                    Exam ob =  Exam.get(i);
                    if (sem.equalsIgnoreCase(ob.getSemestr())){
                        averageRat =averageRat+ob.getGrade();
                        a++;
             } }

        return averageRat/a; }


}