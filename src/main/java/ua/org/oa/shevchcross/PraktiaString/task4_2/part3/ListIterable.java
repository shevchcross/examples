package ua.org.oa.shevchcross.PraktiaString.task4_2.part3;

/**
 * Created by Olya on 12.05.2018.
 */
public interface ListIterable<E> { ListIterator<E> listIterator(); }
