package ua.org.oa.shevchcross.PraktiaString.task4_Dictionary;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Olya on 15.05.2018.
 */
public class ReadVocabulary {
    final static String EN_RU = "EN-RU.txt";
    final static String RU_EN = "RU-EN.txt";
// основной метод который читает с консоли и запускает остальные методы
    public  void readFromConsole (){
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))){
            System.out.println(" Select vocabulary: " + RU_EN + " or " + EN_RU);
            String vocabP = "src/main/resources/" + reader.readLine();
            while(true){
                if (vocabP.equals("src/main/resources/" +RU_EN)|| vocabP.equals("src/main/resources/" + EN_RU)){
                    break;}
                else {
                    System.out.println("EROR select right vocabulary: " + EN_RU + " or " + RU_EN);
                    vocabP = "src/main/resources/" + reader.readLine(); }

            }
            System.out.println("Please enter path.");
            String path = reader.readLine();
            File textLink;
            while (true){
                textLink = new File(path);
                if (textLink.exists()){
                    break;
                }
                else {
                    System.out.println("File not found. Please check path and try again");
                    path = reader.readLine();}
            }
            String text = readFile(path, "UTF-8");
            String dictionary =readFile(vocabP,"UTF-8" );
            translate(text, dictionary);
        } catch (IOException e) {
            e.printStackTrace(); }
    }

        public void translate (String text, String vocab){
            Map<String, String> translation = new HashMap<String, String>();
            String[] array = vocab.split("[;-;-\n]+");
            for (int i = 0; i <array.length ; i++) {
                if(i%2 ==0){
                    translation.put(array[i], array[i+1]);}}// сделал словарь
                       String [] textarray = text.split("\\W+");
            for (int i = 0; i <textarray.length ; i++) {
                if(translation.containsKey(textarray[i])){
                    textarray[i]=translation.get(textarray[i]);}
                    }
            StringBuilder sb = new StringBuilder();
            for (String string:textarray){
                sb.append(string).append(" "); }
            System.out.println(sb.toString());
        }


    public static String readFile(String path, String charSet) {
        StringBuilder sb = new StringBuilder();
        String s = null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(path),charSet))){
            while ((s = reader.readLine()) != null)
                sb.append(s).append("\n");
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return sb.toString();
    }}


