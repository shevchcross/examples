package ua.org.oa.shevchcross.PraktiaString.Praktika4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Olya on 07.05.2018.
 */
public class App {
    public static void main(String[] args) {
StudentUtils studentUtils = new StudentUtils();
        List<Student> students = new ArrayList<>();

        students.add(new Student("Andrey","Gonchar",5));
        students.add(new Student("Vladimir", "Ivanov", 5));
        students.add(new Student("Aleksandr","Dubovik",4));
        students.add(new Student("Evgeniy", "Fokin", 4));
        students.add(new Student("Nastia", "Girchenko", 3));
        students.add(new Student("Olesia", "Makova", 3));


        StudentUtils.createMapFromList(students).entrySet().forEach(System.out::println);
        StudentUtils.printStudents(students,5);
        StudentUtils.sortStudent(students).forEach(System.out::println);
         }
}
