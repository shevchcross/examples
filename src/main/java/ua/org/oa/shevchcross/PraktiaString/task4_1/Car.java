package ua.org.oa.shevchcross.PraktiaString.task4_1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Olya on 04.05.2018.
 */

public class Car {
    int value;
    String type;

    public Car(int value, String type) {
        this.value = value;
        this.type = type;
    }
}
