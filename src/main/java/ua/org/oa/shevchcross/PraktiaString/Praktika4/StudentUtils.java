package ua.org.oa.shevchcross.PraktiaString.Praktika4;

import java.util.List;
import java.util.Map;
import java.util.*;

/**
 * Created by Olya on 07.05.2018.
 */
public class StudentUtils {



  static  Map<String, Student> createMapFromList(List<Student> students){
        String k;
        Student s;
        Map<String, Student> mapStudents = new HashMap<String, Student>();
        for (int i = 0; i < students.size(); i++) {
             s = students.get(i);
             k = s.getFirstName()+s.getLastName();
            mapStudents.put(k,s);}
             return mapStudents;}

static void printStudents(List<Student> students, int course){

      for(Iterator<Student> iterator=students.iterator(); iterator.hasNext();)
      { Student s = iterator.next();
        if (s.getCourse() == course)
            System.out.println(s.getFirstName()+" "+ s.getLastName());
        }}

   static List<Student> sortStudent(List<Student> students){
       List<Student> sortStudent = new ArrayList<>(students);
       sortStudent.sort((a, b) -> a.getFirstName().compareTo(b.getFirstName()));
       return sortStudent;
   }

}





