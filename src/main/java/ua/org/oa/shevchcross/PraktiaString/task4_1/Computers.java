package ua.org.oa.shevchcross.PraktiaString.task4_1;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Olya on 04.05.2018.
 */

public class Computers implements Comparable<Computers> {
    String system;
    String brend;
    int value;

    public Computers(String system, String brend, int value) {
        this.system = system;
        this.brend = brend;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Computers{" +
                "system='" + system + '\'' +
                ", brend='" + brend + '\'' +
                ", value=" + value +
                '}';
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getBrend() {
        return brend;
    }

    public void setBrend(String brend) {
        this.brend = brend;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public int compareTo(Computers o) {
        return Double.compare(o.getValue(),this.getValue() );
    }
}
