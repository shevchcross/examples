package ua.org.oa.shevchcross.PraktiaString.Praktika6;

/**
 * Created by Olya on 04.06.2018.
 */
public class MyThread extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {setName("MyThread");
                System.out.println(getName() + " " + i);
                Thread.sleep(500);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}}
