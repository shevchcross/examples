package ua.org.oa.shevchcross.PraktiaString.Praktika4.Ex5;

/**
 * Created by Olya on 07.05.2018.
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Text {
    static String patch = "c:\\Users\\Olya\\Desktop\\TextForEx5.txt";

    private static String readFile(String patch, String charSet){
        StringBuilder stringBuilder = new StringBuilder();
        String s = null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(patch), charSet))) {
            while ((s = reader.readLine()) != null){
                stringBuilder.append(s).append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
    public static String parsing(){
        String s = "";
        Pattern pattern = Pattern.compile("<td>(?<rate>\\d+)</td><td>(?<male>.{2,})</td><td>(?<female>.{2,})</td>");
        Matcher matcher = pattern.matcher(readFile(patch, "windows-1251"));
        while (matcher.find()){
            s = s.concat(matcher.group() + ".");
        }
        return s;
    }
    public static void main(String[] args) {
        System.out.println(Text.readFile(patch, "windows-1251"));

    }

}
