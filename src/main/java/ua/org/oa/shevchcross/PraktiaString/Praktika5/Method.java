package ua.org.oa.shevchcross.PraktiaString.Praktika5;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Olya on 17.05.2018.
 */
public class Method {

    public void write(String What, String to) {

        try (FileWriter writer = new FileWriter(to);) {
            writer.write(What);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String random(int fromNamber, int toNamber, int valueNamber) {
        int namber = 0;
        int arrayNamber[];
        String str = "";
        arrayNamber = new int[valueNamber];
        for (int i = 0; i < valueNamber; i++) {
            namber = fromNamber + (int) (Math.random() * toNamber);
            arrayNamber[i] = namber;
        }
        for (int i = 0; i < valueNamber; i++) {
            str = str.concat(Integer.toString(arrayNamber[i]) + "\n");
        }
        return str;
    }

    public String readFile(String from) {
        StringBuilder sb = new StringBuilder();
        try (FileReader reader = new FileReader(from)) {
            int code = 0;
            while ((code = reader.read()) != -1) {
                sb.append((char) code);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public String sort(String s) {
        StringBuilder sb = new StringBuilder();
        String string = "";
        ArrayList<Integer> list = new ArrayList();
        String[] ar = s.split("\\n");
        for (String str : ar) {
            list.add(Integer.parseInt(str));
        }
        list.sort((a, b) -> b.compareTo(a));

        for (Integer in : list) {
            sb.append(in).append("\n");
        }
        return sb.toString();

    }
}