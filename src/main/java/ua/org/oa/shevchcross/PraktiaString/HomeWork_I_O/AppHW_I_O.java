package ua.org.oa.shevchcross.PraktiaString.HomeWork_I_O;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * Created by Olya on 06.06.2018.
 */
public class AppHW_I_O {
    public static void main(String[] args) {

        try {
            System.out.println(createList("C:\\Users\\Olya\\Desktop\\ListBook.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

//serializeHuman("C:\\Users\\Olya\\Desktop\\ListBook1.txt", new Book("qwerty","asdfg", 1234), new Book("zxcvb","poiuy", 4321));
    // serializeBook("C:\\Users\\Olya\\Desktop\\ListBook1.txt", new Book("qwer","asdf",123));
}

    public static  <T> void serializeBook (String path, T... objekt){
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path))) {
            for (T t : objekt) {
                oos.writeObject(t);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

        public static List<Book> deserializeBook (String path){
        List<Book> bookList = new ArrayList<Book>();
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));){
            while (true){
                //System.out.println(ois);
                bookList.add((Book)ois.readObject());
            }
        } catch (EOFException e){

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return bookList; }

        public static List<Book> createList (String path) throws FileNotFoundException {
        List<Book> bookList = new ArrayList<Book>();
        FileInputStream fis = new  FileInputStream(path);
        Pattern patt = Pattern.compile("(?<title>.+?);(?<author>.+?);(?<year>\\d+)");
        String str = null;
                try(BufferedReader reader = new BufferedReader(new InputStreamReader(fis))){
                      while ((str=reader.readLine())!=null){
                          Matcher matcher = patt.matcher(str);
                          while (matcher.find()){
                              Book book = new Book(matcher.group("title"), matcher.group("author"), Integer.parseInt(matcher.group("year")));
                              bookList.add(book);
                          }
                         }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bookList;
        }



    }



