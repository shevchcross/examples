package ua.org.oa.shevchcross.PraktiaString.HomeWorkString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.util.stream.Collectors.joining;

public class App {
    public static void main(String[] args) {
    String s = "123456789";
        System.out.println(new App().reversString(s)); }

            public String reversString(String s) {
                        return new StringBuffer(s).reverse().toString();
            }

            public boolean palindrome(String s) {
                String s1 = "[\\p{Punct}]|[\\s]";
                return  new StringBuffer(s.replaceAll(s1, "")).reverse().toString()
                        .equalsIgnoreCase(s.replaceAll(s1, ""));
            }

            public String checksTheLengthOfaString(String s){
                char [] ar1 = new char[6];
                char [] ar2 = {'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o'};
                char [] array = s.toCharArray();
                if (10 < array.length){
                    System.arraycopy(array, 0, ar1, 0, 6);
                    return new String(ar1);
                }else{
                    System.arraycopy(array, 0, ar2, 0, array.length);
                    return  new String(ar2);
                }
            }

            public String swapsTheFirstLastWordInString(String s) {
                String[] words = s.split(" ");
                String temp = words[words.length - 1];
                words[words.length - 1] = words[0];
                words[0] = temp;
                return Arrays.stream(words).collect(joining(" "));
            }

            public String  firstAndLastWordInEachSentence(String s) {
                String[] str = s.split("[\\.]\\s?");
                String str1 = "";
                for (String s1 : str) {
                    String[] words = s1.split(" ");
                    String temp = words[words.length - 1];
                    words[words.length - 1] = words[0];
                    words[0] = temp;
                    str1 = str1 + Arrays.stream(words).collect(joining(" ")) + ". ";
                }
                return str1;
            }

            public boolean checkTheContentsOfChar(String s) {
                boolean b = false;
                for (char i : s.toCharArray()) {
                    if (i == 'a' || i == 'b' || i == 'c'){
                        b = true;
                    }
                }
                return b;
            }

            public boolean isTheDateLine(String s){
                return Pattern.matches("[0-1][0-2]\\.[0-3]\\d\\.[0-2]\\d\\d\\d", s);
            }

            public boolean lineIsMailingAddress(String s){
                return Pattern.matches("[\\w\\.\\-\\_]+@[\\w\\.]+\\b", s);
            }

            public String findsAllPhones(String s){
                List<String> list = new ArrayList<>();
                Pattern pattern = Pattern.compile("\\+\\d\\(\\d\\d\\d\\)\\d\\d\\d\\-\\d\\d\\-\\d\\d");
                Matcher matcher = pattern.matcher(s);
                while (matcher.find()){
                    list.add(matcher.group());
                }
                return list.toString();
            }

            public void implementMarkdown(){
            }


        }

