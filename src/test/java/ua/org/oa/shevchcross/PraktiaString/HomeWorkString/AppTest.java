package ua.org.oa.shevchcross.PraktiaString.HomeWorkString;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Olya on 17.04.2018.
 */
public class AppTest {

    @Test
    public void reversString() throws Exception {
      String s = "123456789";
       String expected = "987654321";
      String actual = new App().reversString(s);
        assertEquals("Check reversString", expected, actual);}

    @Test
    public void palindrome() throws Exception {
        String s = "А роза упала на лапу Азора";
        Boolean expected = true;
        Boolean actual = new App().palindrome(s);
        assertEquals("Check palindrome", expected, actual);
    }

    @Test
    public void checksTheLengthOfaStringMoreThenTen() throws Exception {
        String s ="А роза упала на лапу Азора";
        String expected = "А роза";
        String actual = new App().checksTheLengthOfaString(s);
        assertEquals("Check checksTheLengthOfaStringMoreThenTen", expected, actual);
    }
    @Test
    public void checksTheLengthOfaStringLessThenTen() throws Exception {
        String s ="А роза уп";
        String expected = "А роза упooo";
        String actual = new App().checksTheLengthOfaString(s);
        assertEquals("Check checksTheLengthOfaStringMoreThenTen", expected, actual);
    }

    @Test
    public void swapsTheFirstLastWordInString() throws Exception {

    }

    @Test
    public void firstAndLastWordInEachSentence() throws Exception {

    }

    @Test
    public void checkTheContentsOfChar() throws Exception {

    }

    @Test
    public void isTheDateLine() throws Exception {

    }

    @Test
    public void lineIsMailingAddress() throws Exception {

    }

    @Test
    public void findsAllPhones() throws Exception {

    }

}