package ua.org.oa.shevchcross.PraktiaString.Praktika1Test;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Olya on 22.04.2018.
 */
public class StudentTest {
    @BeforeClass
    public static void BeforeClass(){
        Student student = new Student("Vania", "Ivanov", 3, "TM");
        student.addExam(new Exam("Math", 4, "2.4.2106", "sammer"));
        student.addExam(new Exam("Math", 2, "7.01.2018", "winter"));
    }
    @Test
    public void addExam() throws Exception {

    }

    @Test
    public void searchMaxGrade() throws Exception, ExamExeption {
        Student student = new Student("Vania", "Ivanov", 3, "TM");
        student.addExam(new Exam("Math", 4, "2.4.2106", "sammer"));
        student.addExam(new Exam("Math", 2, "7.01.2018", "winter"));
        int expected = 4;
        int actual = student.searchMaxGrade("Math");
        assertEquals(expected,actual);

    }

    @Test
    public void delGrade() throws Exception {
        Student student = new Student("Vania", "Ivanov", 3, "TM");
        student.addExam(new Exam("Math", 4, "2.4.2106", "sammer"));
        student.addExam(new Exam("Math", 2, "7.01.2018", "winter"));
        Exam expected = new Exam("Math", 0, "2.4.2106", "sammer");

        student.delGrade("Math","2.4.2106");

    }

    @Test
    public void valueExamWithGrade() throws Exception {
        Student student = new Student("Vania", "Ivanov", 3, "TM");
        student.addExam(new Exam("Math", 4, "2.4.2106", "sammer"));
        student.addExam(new Exam("Math", 2, "7.01.2018", "winter"));
        int expected = 1;
        int actual = student.valueExamWithGrade(4);
        assertEquals(expected,actual);
    }

    @Test
    public void averageRating() throws Exception {
        Student student = new Student("Vania", "Ivanov", 3, "TM");
        student.addExam(new Exam("Math", 4, "2.4.2106", "sammer"));
        student.addExam(new Exam("Math", 2, "7.01.2018", "sammer"));
        double expected = 3.0;
        double actual = student.averageRating("sammer");
        assertEquals(expected,actual,0);
    }

}